---
title: Kontakt/Impressum
---

**Matthias Wimmer**  
Maria-Ivogün-Allee 5  
81245 München

Telefon: [+49-17777-19999](tel:+491777719999)  
E-Mail: <fragen@sportboot.mobi>
