Sailtrainer Website
===================


This repository contains the source code for <https://sportboot.mobi/>


Quickstart
----------

```sh
stack build
stack exec site build
stack exec site watch
```

… navigate to <http://localhost:8000>
