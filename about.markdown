---
title: Über
---

<img src='/images/mawis.jpeg' alt='Foto vom Autor der Apps'
        style='width: 240px; height: 240px; float: right'/>

Die Android-Apps sind entstanden als ich selbst auf die diversen
Sportbootführerscheine gelernt habe. Mein Ziel war es nie damit Geld zu machen,
so dass ich sie ganz gerne kostenlos für andere Wassersportler anbiete.

Wenn du selbst Verbesserungen an den Apps vornehmen willst oder die Apps
für andere Prüfungen hernehmen möchtest, du findest den kompletten Programmcode
als Open Source auf <https://gitlab.com/sailtrainer>.

Natürlich freue ich mich, wenn du Verbesserungen wiederum als Merge-Request
zur Verfügung stellst.
