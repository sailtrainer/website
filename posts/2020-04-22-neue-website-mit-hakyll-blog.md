---
title: Neue Website mit Blog
---

Ich möchte auf dieser Seite in Zukunft darüber berichten was ich so mit den
Sportboot-Trainern mache. Da die Apps „einfach tun” und ich deswegen selten
Updates veröffentlichen muss, sieht es manchmal aus, als wären die Apps nicht
gepflegt. Ich hoffe diese Seite hilft etwas zu zeigen, dass das nicht so ist.

Noch fehlt dieser Seite ein echtes Layout. Vielleicht passt das ja zu den Apps.
Ich werde noch daran arbeiten, aber der Inhalt und die Funktionalität war mir
immer wichtiger als die Optik. Auch gilt hier auf der Website wie in der App,
dass Nutzer nicht überwacht und nicht getrackt werden.
