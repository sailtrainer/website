---
title: Wenn ihr denkt, dass es einen Fehler gibt …
---

Es passieren immer mal Fehler. Davor bin auch ich nicht gefeit, besonders wenn
ich einen Fragenkatalog inklusive falscher Antworten abschreiben muss. Nun gibt
es die Apps schon ein paar Jahre und ich hoffe, die meisten Fehler sind gefunden
(und behoben). Trotzdem kann natürlich immer noch etwas falsch sein. Wenn ihr
meint, dass ihr einen Fehler gefunden habt, dann [sagt mir bitte
Bescheid.](https://sportboot.mobi/contact.html)

Um die Sache ohne große Nachfrage überprüfen zu können, **schreibt aber bitte
in eurem Fehlerbericht dazu um welche App es geht.** Die Regelungen auf
Binnenschifffahrtsstraßen und auf See unterscheiden sich teilweise gerade in den
Details und man spricht sonst schnell aneinander vorbei.

Falls ihr Lust habt, könnt ihr auch selbst überprüfen, ob ich die Frage korrekt
abgeschrieben habe oder nicht. Die amtlichen Fragekataloge gibt es auf
[ELWIS](https://www.elwis.de/) im Bereich
[Sportschifffahrt](https://www.elwis.de/DE/Sportschifffahrt/Sportschifffahrt-node.html)
und dort wiederrum unter
[Sportbootführerscheine](https://www.elwis.de/DE/Sportschifffahrt/Sportbootfuehrerscheine/Sportbootfuehrerscheine-node.html).

Manchmal ist der Grund für eine anscheinend falsche Frage nämlich einfach, dass
die Frage im Katalog schon etwas seltsam ist. Da ich mit den Apps euch nicht
unterrichten sondern in der Prüfung helfen möchte, habe ich alle Fragen genau
so wie sie sind in die App übernommen.
*Die Teilnahme an einem Theoriekurs oder zumindest intensives Selbststudium mit
einem Buch empfehle ich euch auf jeden Fall zusätzlich zum Arbeiten mit meiner
App.*
