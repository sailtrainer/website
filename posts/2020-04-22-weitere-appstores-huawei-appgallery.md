---
title: Sportboot-Trainer in der Huawei AppGallery
---

Seit Februar habe ich die Sportboot-Trainer auch in die [AppGallery von
Huawei](https://huaweimobileservices.com/appgallery/) eingestellt. Die Zeiten
sind vorbei, dass alle Android-Nutzer den Google Play Store nutzen können.
Aufgrund von US-amerikanischen Sanktionen haben neue Huawei-Telefone diesen
nicht mehr.

Andere App-Stores
-----------------

### F-Droid

Meine Apps sind damit jetzt in drei Appstores verfügbar: bei Google, Amazon und
Huawei. Wo könnte ich sie sonst noch einstellen?
[F-Droid](https://f-droid.org/)? Ja, sehr gerne. Aber ich muss dazu noch etwas
an der App arbeiten. Zwar ist schon jetzt alles Open Source, aber für F-Droid
brauche ich auch einen reproducable Build. Das heißt F-Droid baut den
Source-Code ohne ihn zu signieren. Wenn daraus genau der gleiche Binärcode
entsteht, dann übernimmt F-Droid die von mir gebaute App mit meiner Signatur.

Hier liegt gerade das Problem. So wie ich Gradle gerade einsetze läuft keiner
der Build-Targets wenn kein Signaturkey im Dateisystem liegt. Wenn jemand Lust
hat daran zu arbeiten, [nur
zu](https://gitlab.com/sailtrainer/sbfs-trainer/-/issues/1).


### Galaxy Store von Samsung

Wieso nicht auch in den Galaxy Store? Ich erinnere mich, vor einigen Jahren
wurde ich von Samsung explizit eingeladen, meine Apps doch auch bei ihnen
einzustellen. Damals war ich nicht begeistert. Ich dachte, da will Samsung
nur was eigenes machen und Android fragmentieren. Ein zentraler Appstore
schien mir viel besser. Ich verstehe gar nicht mehr, wie ich so eine
marktbeherrschende Stellung eines Anbieters damals gutheißen konnte …

Als ich die App nun doch auch dort einstellen wollte, war ich sehr überrascht:
Samsung scheint gar nicht mehr wirklich einen eigenen Store betreiben zu
wollen. Es werden nur noch Apps angenommen, die spezielle SDKs von Samsung
verwenden, oder wenn man ein Partner ist und seine Apps besonders für Samsung
vermarktet. – Sonderlich viel Angst scheinen die Koreaner also nicht zu haben,
dass ihnen das gleiche Schicksal wie Huawei drohen könnte.


### Chinesische App-Stores

Da Google in China gesperrt ist, betreiben eigentlich alle Android-Hersteller
dort ihren eigenen App Store. Außerdem gibt es Stores von Tencent, Baidu und Co.
Relevant sind diese allerdings wohl nur in China und für dortige Nutzer sind
diese Apps vermutlich nicht von Interesse. Sollte jedoch die [gemeinsame
Plattform von Xiaomi, Vivo und
Oppo](https://www.heise.de/newsticker/meldung/Chinesische-Smartphone-Hersteller-gegen-Google-4655028.html)
kommen, so werde ich sicher nochmals darüber nachdenken.
